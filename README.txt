MoreScreen Notes

1. Arc Support for AFNetworking and MBProgressHUD.

 When adding this code to your own application you will
have to go your Application Target ( i.e. in the demo app the target is 'MoreScreen' )

Project -> Target -> Build Phases -> Compile Sources

Add the following compiler flag:

-fno -objc -arc

to all MBProgressHUD .m files and all AFNetworking .m files

2.  Frameworks needed - CFNetwork - SystemConfiguration - QuartzCore

3.  External APIs - Reachability - MBProgressHUD - AFNetworking

4.  In the MoreAppsTableViewController.m you can set your search text by defining the
following:

#define SEARCH_TEXT_0  	@"Your search Text" 
#define SEARCH_TEXT_1	@"Another search text" 
#define SEARCH_TEXT_2	@"A third search text" 
#define PARTNER_ID		@"YOUR Affiliate Company ID, (i.e. in the U.S. Linkshare's ID is '30')" 
#define SITE_ID			@"YOUR Affiliate ID"

5.  In Search.m  look for the method

- (NSURL *)urlWithSearchText:(NSString *)searchText category:(NSInteger)category -  
the case here is tied to the segmentedControl's index.  So case 0 is equivalent to index 0,
case 1 is equivalent to index 1 

- the categoryName sets the search category in the App Store Web Search API 

- categoryNames can be set to track, audiobook, ebook, and software 

- Read the iTunes Search API for more specific categories: 
http://www.apple.com/itunes/affiliates/resources/documentation/itunes-store-web-service-
search-api.html#searching


