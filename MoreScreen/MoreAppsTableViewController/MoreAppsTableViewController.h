//
//  MoreAppsTableViewController.h
//  MoreScreen
//
//  Created by kakil on 9/1/12.
//  Copyright (c) 2012 AkilDev LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface MoreAppsTableViewController : UITableViewController <MBProgressHUDDelegate>

@end
