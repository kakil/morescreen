//
//  MoreAppsTableViewController.m
//  MoreScreen
//
//  Created by kakil on 9/1/12.
//  Copyright (c) 2012 AkilDev LLC. All rights reserved.
//

#import "MoreAppsTableViewController.h"
#import "Search.h"
#import "SearchResult.h"
#import "SearchResultCell.h"
#import "AFJSONRequestOperation.h"
#import "AFImageCache.h"
#import <QuartzCore/QuartzCore.h>

//Here we will define the search text for each segment
//You can define these any way you like
//
#define SEARCH_TEXT_0       @"AkilDev LLC"              //I use my company name here to bring back all of my apps
#define SEARCH_TEXT_1       @"Celebrity Gossip"         //This search text searches for related apps
#define SEARCH_TEXT_2       @"Top Apps"                 //This text searches for top apps

//Here we define the Linkshare details for your account
//If you are outside the U.S. you will not use Linkshare
//
#define PARTNER_ID          @"30"                       //This is the Linkshare ID different if another country
#define SITE_ID             @"1781692"                  //Your affiliate ID

static NSString *const NothingFoundCellIdentifier = @"NothingFoundCell";

@interface MoreAppsTableViewController ()

@property (nonatomic, weak) IBOutlet UISegmentedControl *segmentedControl;
@property (nonatomic, strong) MBProgressHUD * HUD;

- (IBAction)segmentChanged:(UISegmentedControl *)sender;

@end

@implementation MoreAppsTableViewController {
	Search *search;
    BOOL reloading;
}

@synthesize segmentedControl    = _segmentedControl;
@synthesize HUD                 = _HUD;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)doneLoadingTableViewData{
	reloading = NO;
}

- (void)initLoader
{
    self.HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.HUD];
    
    self.HUD.delegate = self;
    self.HUD.dimBackground = YES;
    self.HUD.labelText = @"Loading";
    self.HUD.detailsLabelText = @"updating data";
    self.HUD.square = YES;
    
    [self.HUD show:YES];
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UINib * cellNib = [UINib nibWithNibName:NothingFoundCellIdentifier bundle:nil];
	[self.tableView registerNib:cellNib forCellReuseIdentifier:NothingFoundCellIdentifier];
    
    [self performSearch];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - MBProgressHUDDelegate methods

- (void)hudWasHidden:(MBProgressHUD* )hud
{
    [self.HUD removeFromSuperview];
    self.HUD = nil;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSLog(@"%d", [search.searchResults count]);
    return [search.searchResults count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * SearchResultCellIdentifier = @"SearchResultCell";
    
    // Configure the cell...
    SearchResultCell *cell = (SearchResultCell *)[tableView dequeueReusableCellWithIdentifier:SearchResultCellIdentifier];
    
    SearchResult *searchResult = [search.searchResults objectAtIndex:indexPath.row];
    [cell configureForSearchResult:searchResult];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 96;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SearchResult *searchResult = [search.searchResults objectAtIndex:indexPath.row];
    NSLog(@"Store URL = %@", searchResult.storeURL);
    
    NSString * iTunesLink = [NSString stringWithFormat:@"%@&partnerId=%@&siteID=%@", searchResult.storeURL, PARTNER_ID, SITE_ID];
    [iTunesLink stringByReplacingOccurrencesOfString:@"http" withString:@"itms"];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
}

#pragma mark - UISearchBarDelegate

- (void)showNetworkError
{
	UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:NSLocalizedString(@"Whoops...", @"Error alert: title")
                              message:NSLocalizedString(@"There was an error reading from the iTunes Store. Please try again.", @"Error alert: message")
                              delegate:nil
                              cancelButtonTitle:NSLocalizedString(@"OK", @"OK button")
                              otherButtonTitles:nil];
    
	[alertView show];
}

- (void)performSearch
{
    [self initLoader];
    
	search = [[Search alloc] init];
	//NSLog(@"allocated %@", search);
    NSString * searchText;
    
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        searchText = SEARCH_TEXT_0;
    }else if (self.segmentedControl.selectedSegmentIndex == 1) {
        searchText = SEARCH_TEXT_1;
    }else if (self.segmentedControl.selectedSegmentIndex == 2) {
        searchText = SEARCH_TEXT_2;
    }
    
    [search performSearchForText:searchText
                        category:self.segmentedControl.selectedSegmentIndex
                      completion:^(BOOL success) {
                          if (!success) {
                              [self showNetworkError];
                          }
                          
                          [self.tableView reloadData];
                          [self doneLoadingTableViewData];
                          
                          self.HUD.labelText = @"Completed";
                          [self.HUD hide:YES];
                          
                      }];
    
	[self.tableView reloadData];
}

- (IBAction)segmentChanged:(UISegmentedControl *)sender
{
    
	[self performSearch];
}


@end
