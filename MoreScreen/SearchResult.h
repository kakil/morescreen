//
//  SearchResult.h
//  the gossip
//
//  Created by kakil on 7/25/12.
//  Copyright (c) 2012 AkilDevLLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchResult : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *artistName;
@property (nonatomic, copy) NSString *artworkURL60;
@property (nonatomic, copy) NSString *artworkURL100;
@property (nonatomic, copy) NSString *storeURL;
@property (nonatomic, copy) NSString *kind;
@property (nonatomic, copy) NSString *currency;
@property (nonatomic, copy) NSDecimalNumber *price;
@property (nonatomic, copy) NSString *genre;
@property (nonatomic, copy) NSString *description;

- (NSComparisonResult)compareName:(SearchResult *)other;

- (NSString *)kindForDisplay;

@end
