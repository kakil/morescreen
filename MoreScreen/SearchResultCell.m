//
//  SearchResultCell.m
//  the gossip
//
//  Created by kakil on 7/25/12.
//  Copyright (c) 2012 AkilDevLLC. All rights reserved.
//

#import "SearchResultCell.h"
#import "SearchResult.h"
#import "UIImageView+AFNetworking.h"

@implementation SearchResultCell

@synthesize nameLabel = _nameLabel;
@synthesize artistNameLabel = _artistNameLabel;
@synthesize artworkImageView = _artworkImageView;

- (void)awakeFromNib
{
	[super awakeFromNib];

	UIImage *image = [UIImage imageNamed:@"TableCellGradient"];
	UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:image];
	self.backgroundView = backgroundImageView;

	UIImage *selectedImage = [UIImage imageNamed:@"SelectedTableCellGradient"];
	UIImageView *selectedBackgroundImageView = [[UIImageView alloc] initWithImage:selectedImage];
	self.selectedBackgroundView = selectedBackgroundImageView;
}

- (void)configureForSearchResult:(SearchResult *)searchResult
{
	self.nameLabel.text = searchResult.name;

	NSString *artistName = searchResult.artistName;
	if (artistName == nil) {
		artistName = NSLocalizedString(@"Unknown", @"Unknown artist name");
	}

	//NSString *kind = [searchResult kindForDisplay];
	//self.artistNameLabel.text = [NSString stringWithFormat:NSLocalizedString(@"ARTIST_NAME_LABEL_FORMAT", @"Format for artist name label"), searchResult.description, kind];
    self.artistNameLabel.text = [NSString stringWithFormat:@"%@", searchResult.description];
	[self.artworkImageView setImageWithURL:[NSURL URLWithString:searchResult.artworkURL60] placeholderImage:[UIImage imageNamed:@"Placeholder"]];
}

- (void)prepareForReuse
{
	[super prepareForReuse];
	[self.artworkImageView cancelImageRequestOperation];
	self.nameLabel.text = nil;
	self.artistNameLabel.text = nil;
}

@end
