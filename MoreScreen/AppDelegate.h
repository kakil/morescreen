//
//  AppDelegate.h
//  MoreScreen
//
//  Created by kakil on 8/31/12.
//  Copyright (c) 2012 AkilDev LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
